import greenfoot.*;  

/**
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
public class Snake extends Actor {
    
	public Snake() {
		start();
	}

    public void start(){
        start_();
    }
    private native void start_();
    
    public void act(){
        act_();
    }
    private native void act_();
    
    public void setAngle(int angle){
        setAngle_(angle);
    }
    private native void setAngle_(int angle);
    
    public void setDistance(int distance){
        setDistance_(distance);
    }
    private native void setDistance_(int distance);
    
    static {
        System.load(new java.io.File(".jni", "Snake_jni.so").getAbsolutePath());
    }
}
